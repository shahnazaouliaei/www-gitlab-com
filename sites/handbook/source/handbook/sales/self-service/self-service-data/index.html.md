---
layout: handbook-page-toc
title: "Self-Service Data Team at GitLab"
description: "The Self-Service Data Team is responsible for leveraging data to optimize for the self-service customer experience and drive nARR growth via sales efficiency. Data insights from this team feed: sales visibility, self-service fulfillment features, and growth/marketing experiments."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## Welcome to the Self-Service Data Team Handbook

**The Self-Service Data Team is responsible for leveraging data to optimize for the self-service customer experience and drive nARR growth via sales efficiency. Data insights from this team feed: sales visibility, self-service fulfillment features, and growth/marketing experiments.** 
{: .alert .alert-success}


## Team

|  **Name** | **GitLab Handle** | **Title** |
| :--------------- | :----------------- | :----------------- |
| Alex Martin | @alex_martin | Sr. Mgr, Self-Service & Online Sales Data |
| Max Fleisher | @mfleisher |  Mgr, Self-Service & Online Sales Data |
| Sara Gladchun | @sglad | Sr. Analyst, Self-Service & Online Sales Data |

## Resources

|  **Resource** | **About** |
| :--------------- | :----------------- | 
| [Primary Epic](https://gitlab.com/groups/gitlab-com/sales-team/-/epics/50) | Contains OKRs, prioritized work, and weekly status updates |
| [Data Request Issue Template](https://gitlab.com/gitlab-com/sales-team/self-service/-/issues/102) | Template that should be copied for ad-hoc data questions and requests | 
| [Data Hub](https://docs.google.com/document/d/10p86n7f5vt4UmhHM4ZGRZm4OSa5k5g-LKQ0uBAKnvSc/edit?usp=sharing) | All of our data assets and resources in one place | 


## Working with us
**Purpose**: outline how the broader Self-Service team can engage the Self-Service Data Squad (Alex, Max, Sara)

**Goal**: minimize dependencies/blockers to insights while providing transparent engagement model

**Disclaimer**: Not all data questions will be able to be answered. Ultimately, taking time to answer ad-hoc questions means less time on projects (aka the zero-sum capacity problem). That is not to say that ad-hoc questions are not important; however, we do have “boulder” level projects in flight have been prioritized via the OKR process, which we also need to make progress on.

**How to submit your ad-hoc data request or question**:

1. Have you tried to answer this question leveraging existing resources (e.g. data hub, SFDC)?
    a. If no, please try to answer your question using these existing resources.
    b. If yes, but you’re still unable to answer your question, go to question 2.

2. Using [this template](https://gitlab.com/gitlab-com/sales-team/self-service/-/issues/102), please clone and:
    a. Fill out all items under the “Filled out by Requestor” section
    b. Add the “Self-Service Data” and “Self-Service Data Ad Hoc” labels
    c. If business stopping: tag Alex in slack (ideally in “self-service_squad” channel) with link to issue.

## How we prioritize ad hoc requests
The more points the better!

1. Can this question be incorportated into existing OKR? If yes, +3
2. Ease of ability to answer question (+0 = 8+ hours; +1 = 4-8 hours; +2 = 2-3 hours; +3 = 1 hour or less)
3. Priority (High = +3, Medium = +2, Low = +1)
4. Is an c-suite member asking for this? If yes, +2

If an ad-hoc request scores north of 7 points, we will re-consider prioritizing it above existing OKR work.


